﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace DevmodelsExplorer.Validators
{
    public class ValueNotEmptyValidator : ValidationRule
    {
        public override ValidationResult Validate
          (object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value == null || value.ToString().Trim() == "")
                return new ValidationResult(false, "Wartość musi zostać wypełniona");
          
            return ValidationResult.ValidResult;
        }
    }
}
