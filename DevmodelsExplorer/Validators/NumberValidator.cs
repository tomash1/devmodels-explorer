﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace DevmodelsExplorer.Validators
{
    public class NumberValidator : ValidationRule
    {
        public override ValidationResult Validate
          (object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value != null && value.ToString().Trim() != "")
            {
                try
                {
                    int.Parse(value.ToString());
                }
                catch (Exception ex)
                {
                    try
                    {
                        double.Parse(value.ToString());
                    }
                    catch (Exception e)
                    {
                        return new ValidationResult(false, "Wartość musi być liczbą");
                    }
                    return new ValidationResult(false, "Wartość musi być liczbą całkowitą");
                }
            }
            return ValidationResult.ValidResult;
        }
    }

    public class NumberValidatorNotEmpty : ValidationRule
    {
        public override ValidationResult Validate
          (object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value == null || value.ToString().Trim() == "")
                return new ValidationResult(false, "Wartość nie może być pusta");
            else
            {
                try
                {
                    var parsedInt = 0;
                    int.Parse(value.ToString());
                }
                catch (Exception ex)
                {
                    try
                    {
                        double.Parse(value.ToString());
                    }
                    catch (Exception e)
                    {
                        return new ValidationResult(false, "Wartość musi być liczbą");
                    }
                    return new ValidationResult(false, "Wartość musi być liczbą całkowitą");
                }

            }
            return ValidationResult.ValidResult;
        }
    }
}
