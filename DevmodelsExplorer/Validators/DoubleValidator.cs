﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace DevmodelsExplorer.Validators
{
    public class DoubleValidator : ValidationRule
    {
        public override ValidationResult Validate
          (object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value != null && value.ToString().Trim() != "")
            {
                try
                {
                    double.Parse(value.ToString(), CultureInfo.InvariantCulture);
                }
                catch (Exception ex)
                {
                    return new ValidationResult(false, "Wartość musi być liczbą");
                }
            }

            return ValidationResult.ValidResult;
        }
    }

    public class DoubleValidatorNotEmpty : ValidationRule
    {
        public override ValidationResult Validate
          (object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value == null || value.ToString().Trim() == "")
                return new ValidationResult(false, "Wartość nie może być pusta");
            else
            {
                try
                {
                    double.Parse(value.ToString(), CultureInfo.InvariantCulture);
                }
                catch (Exception ex)
                {
                    return new ValidationResult(false, "Wartość musi być liczbą");
                }

            }
            return ValidationResult.ValidResult;
        }
    }
}
