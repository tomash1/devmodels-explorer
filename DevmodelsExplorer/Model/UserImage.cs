﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace DevmodelsExplorer.Model
{
    public enum ImageFileType { JPG=0, PNG=1, NOT_SUPPORTED=2 }

    public class UserImage
    {

        #region Fields

        #endregion

        #region Properties

        public ImageFileType Type { get; }
        public string Path { get; }

        public string FileName => System.IO.Path.GetFileName(Path);

        public BitmapSource Miniature { get; set; }

        #endregion

        #region Constructor

        public UserImage(string path, ImageFileType type)
        {
            this.Path = path;
            this.Type = type;
        }

        #endregion

        public static ImageFileType GetType(string extension)
        {
            switch(extension.ToLower())
            {
                case "jpg":
                    return ImageFileType.JPG;
                case "png":
                    return ImageFileType.PNG;
                default:
                    return ImageFileType.NOT_SUPPORTED;
            }
        }
    }
}
