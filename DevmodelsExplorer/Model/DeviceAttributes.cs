﻿namespace DevmodelsExplorer.Model
{
    public enum AttributeType { Int, Real, String, Enum }

    public class DeviceAttribute
    {
        public string Classifier {get; set;}
        public string Attribute { get; set; }
        public AttributeType Type { get; set; }
    }

    public static class DeviceClassifiers
    {
        public const string BaseDevice = "::Base Device";

        public const string TechnicalDevice = "::Technical Device";
        public const string NetworkDevice = "::Network Device";
        public const string PowerConsumer = "::Power Consumer";

        public const string RackMounted = "::Rack Mounted";
        public const string ExpansionModule = "::Technical Device Expansion Module";
        public const string ModularDevice = "::Modular Device";
    }

    public static class DeviceAttributes
    {
        public static DeviceAttribute Depth = new DeviceAttribute()
        { Classifier = "::2-Dimensioned Item", Attribute = "Depth", Type = AttributeType.Int};
        public static DeviceAttribute Width = new DeviceAttribute()
        { Classifier = "::2-Dimensioned Item", Attribute = "Width", Type = AttributeType.Int };
        public static DeviceAttribute Height = new DeviceAttribute()
        { Classifier = "::3-Dimensioned Item", Attribute = "Height", Type = AttributeType.Int };
        public static DeviceAttribute HeightInScrews = new DeviceAttribute()
        { Classifier = "::Rack Mounted", Attribute = "HeightInScrews", Type = AttributeType.Int };
        public static DeviceAttribute Weight = new DeviceAttribute()
        { Classifier = "::Weighted Item", Attribute = "Weight", Type = AttributeType.Real };

        public static DeviceAttribute Model = new DeviceAttribute()
        { Classifier = "::Branded Item", Attribute = "Model", Type = AttributeType.String };
        public static DeviceAttribute Manufacturer = new DeviceAttribute()
        { Classifier = "::Branded Item", Attribute = "Manufacturer", Type = AttributeType.String };

        public static DeviceAttribute RatedCurrent = new DeviceAttribute()
        { Classifier = "::Power Consumer", Attribute = "RatedCurrent", Type = AttributeType.Real };
        public static DeviceAttribute PowerRated = new DeviceAttribute()
        { Classifier = "::Power Consumer", Attribute = "Power Rated", Type = AttributeType.Real };
        public static DeviceAttribute HeatRated = new DeviceAttribute()
        { Classifier = "::Power Consumer", Attribute = "Heat Rated", Type = AttributeType.Real };

        public static DeviceAttribute SlotX = new DeviceAttribute()
        { Classifier = "::Technical Device Expansion Module", Attribute = "SlotX", Type = AttributeType.Int };
        public static DeviceAttribute SlotY = new DeviceAttribute()
        { Classifier = "::Technical Device Expansion Module", Attribute = "SlotY", Type = AttributeType.Int };
        public static DeviceAttribute CanRotate = new DeviceAttribute()
        { Classifier = "::Technical Device Expansion Module", Attribute = "CanRotate", Type = AttributeType.Int };

        public static DeviceAttribute PortStatus = new DeviceAttribute()
        { Classifier = "::Generic Port", Attribute = "Status", Type = AttributeType.Enum };
        public static DeviceAttribute PortName = new DeviceAttribute()
        { Classifier = "::Generic Port", Attribute = "Name", Type = AttributeType.String };

        public static DeviceAttribute NetworkPortSpeed = new DeviceAttribute()
        { Classifier = "::Network Port", Attribute = "Speed", Type = AttributeType.Enum };
        public static DeviceAttribute NetworkPortType = new DeviceAttribute()
        { Classifier = "::Network Port", Attribute = "Type", Type = AttributeType.Enum };
        public static DeviceAttribute NetworkPortLocationInDevice = new DeviceAttribute()
        { Classifier = "::Network Port", Attribute = "LocationInDevice", Type = AttributeType.Int };
        public static DeviceAttribute NetworkPortParentId = new DeviceAttribute()
        { Classifier = "::Network Port", Attribute = "ParentID", Type = AttributeType.Int };
    }
}
