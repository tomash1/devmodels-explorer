﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevmodelsExplorer.Tools;

namespace DevmodelsExplorer.Model
{
    public class Device
    {
        private readonly List<string> _implementsL = new List<string>();
        private readonly List<string> _extendsL = new List<string>();

        public string FilePath { get; set; }

        public string Name { get; set; } = "Model name";
        public string Manufacturer { get; set; } = "Manufacturer";
        public string Implements { get; private set; } = "";
        public string Extends { get; private set; } = "";

        public UserImage FrontIcon3D { get; private set; }
        public UserImage BackIcon3D { get; private set; }

        public UserImage Icon2D { get; private set; }

        public void AddImplements(string implements)
        {
            if (!_implementsL.Contains(implements))
                _implementsL.Add(implements);
            Implements += " | " + implements;
        }

        public void AddExtends(string extends)
        {
            if (!_extendsL.Contains(extends))
                _extendsL.Add(extends);
            Extends += " | " + extends;
        }

        public void Add3DFrontIcon(Dictionary<string, string> definition)
        {
            if (definition == null) return;
            string path = null;
            definition.TryGetValue("PATH", out path);
            if (path == null) return;
            var imagePath = NormalizePath(path);
            FrontIcon3D = new UserImage(imagePath, ImageFileType.PNG);
        }

        public void Add3DBackIcon(Dictionary<string, string> definition)
        {
            if (definition == null) return;
            string path = null;
            definition.TryGetValue("PATH", out path);
            if (path == null) return;
            var imagePath = NormalizePath(path);
            BackIcon3D = new UserImage(imagePath, ImageFileType.PNG);
        }

        public void Add2DIcon(Dictionary<string, string> definition)
        {
            if (definition == null) return;
            string path = null;
            definition.TryGetValue("PATH", out path);
            if (path == null) return;
            var imagePath = NormalizePath(path);
            Icon2D = new UserImage(imagePath, ImageFileType.PNG);
        }

        private static string NormalizePath(string imagePath)
        {
            var storagePath = ResourceManager.GetStoragePath();
            var toNormalize = storagePath + imagePath;
            var normalized = Path.GetFullPath(toNormalize);
            return normalized;
        }
    }
}
