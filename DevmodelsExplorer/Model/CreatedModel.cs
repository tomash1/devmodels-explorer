﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevmodelsExplorer.Model
{
    public class CreatedModel
    {
        public string FileName { get; set; }

        public string Manufacturer { get; set; }

        public string FrontIconPath { get; set; }

        public string FrontIconFileName { get; set; }

        public string BackIconPath { get; set; }

        public string BackIconFileName { get; set; }

        public string FilePath { get; set; }

        public bool IsFrontIconSet()
        {
            return FrontIconPath != null && FrontIconPath.Trim() != ""; 
        }

        public bool IsBackIconSet()
        {
            return BackIconPath != null && BackIconPath.Trim() != "";
        }

        public bool isOneOfIconSet()
        {
            return IsFrontIconSet() || IsBackIconSet();
        }
    }
}
