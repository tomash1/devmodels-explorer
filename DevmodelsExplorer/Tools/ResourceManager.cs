﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Win32;
//using System.Windows.Forms;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

namespace DevmodelsExplorer.Tools
{
    public enum ResourceManagerFileType { JPG, PNG, GIF }

    class ResourceManager
    {
        public static string GetStoragePath()
        {
            var devmodelsPath = Properties.Settings.Default.SelectedPath;
            var storagePath = Path.GetFullPath(Path.Combine(devmodelsPath, @"..\..\"));
            return storagePath;
        }

        public static string GetBlobPath()
        {
            var storagePath = GetStoragePath();
            return storagePath + "att.blobs\\manufacturer";
        }

        private static string NormalizeFileName(string onPath)
        {
            var fileName = Path.GetFileName(onPath);
            var newFileName = fileName?.Replace(" ", "_");
            if (newFileName == null) return null;
            newFileName = Regex.Replace(newFileName, @"\u00A0", "_");
            var directory = Path.GetDirectoryName(onPath);
            var newPath = directory + "\\" + newFileName;
            System.IO.File.Move(onPath, newPath);
            return newPath;
        }

        public static string GetIconPath(string selectedPath = null)
        {
            var filters = new ResourceManagerFileType[] { ResourceManagerFileType.PNG, ResourceManagerFileType.JPG };
            var path = ResourceManager.GetFilePathFromOpenDialog(ResourceManagerFileType.JPG, filters, selectedPath);
            var nPath = NormalizeFileName(onPath: path);
            return nPath;
        }

        public static Uri Get(string resourcePath)
        {
            var uri = $"pack://application:,,,/{Assembly.GetExecutingAssembly().GetName().Name};component/{resourcePath}";
            return new Uri(uri);
        }

        public static Uri GetImage(string resourcePath)
        {
            var uri = $"pack://application:,,,/{Assembly.GetExecutingAssembly().GetName().Name};component/Assets/Images/{resourcePath}";
            return new Uri(uri);
        }

        public static IEnumerable<string> ReadFile(string path)
        {
            var fContent = File.ReadAllLines(path);
            return new List<string>(fContent);
        }

        public static string GetDirectoryPathFromOpenDialog()
        {
            var fbd = new System.Windows.Forms.FolderBrowserDialog
            {
                SelectedPath = Properties.Settings.Default.SelectedPath
            };
            var result = fbd.ShowDialog();
            if (result != System.Windows.Forms.DialogResult.OK)
                return null;

            return !string.IsNullOrWhiteSpace(fbd.SelectedPath) ? fbd.SelectedPath : null;
        }

        private static string GetFilter(IEnumerable<ResourceManagerFileType> filteres)
        {
            var filters = new List<string>();
            foreach (var fileType in filteres)
            {
                switch (fileType)
                {
                    case ResourceManagerFileType.JPG:
                        filters.Add("JPEG Files (*.jpeg)|*.jpeg|JPG Files (*.jpg)|*.jpg");
                        break;
                    case ResourceManagerFileType.PNG:
                        filters.Add("PNG Files (*.png)|*.png");
                        break;
                    case ResourceManagerFileType.GIF:
                        filters.Add("GIF Files (*.gif)|*.gif");
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            return filters.Count == 0 ? null : string.Join("|", filters);
        }

        private static string ConvertManagerFileTypeToStringExtension(ResourceManagerFileType t)
        {
            switch (t)
            {
                case ResourceManagerFileType.JPG:
                    return ".jpg";
                case ResourceManagerFileType.PNG:
                    return ".png";
                case ResourceManagerFileType.GIF:
                    return ".gif";
                default:
                    throw new ArgumentOutOfRangeException(nameof(t), t, null);
            }
        }

        private static int GetDefaultExtAsFilterIndex(FileDialog dialog)
        {
            var ext = "*." + dialog.DefaultExt;
            var filter = dialog.Filter;
            var filters = filter.Split('|');
            for (var i = 1; i < filters.Length; i += 2)
            {
                if (filters[i] == ext)
                    return 1 + (i - 1) / 2;
            }
            return 0;
        }

        public static string GetFilePathFromOpenDialog(ResourceManagerFileType defaultType, IEnumerable<ResourceManagerFileType> filters, string selectedPath)
        {

            var fileDialog = new OpenFileDialog()
            {
                DefaultExt = ConvertManagerFileTypeToStringExtension(defaultType),
                Filter = GetFilter(filters),
            };
            if (selectedPath != null)
            {
                var dirPath = System.IO.Path.GetDirectoryName(selectedPath);
                if (System.IO.Directory.Exists(dirPath))
                    fileDialog.InitialDirectory = dirPath;
            }
            fileDialog.FilterIndex = GetDefaultExtAsFilterIndex(fileDialog);
            var showDialog = fileDialog.ShowDialog();
            if (showDialog != null && (bool) showDialog)
            {
                return fileDialog.FileName;
            }
            return null;
        }

    }
}
