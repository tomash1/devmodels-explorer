﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevmodelsExplorer.Tools
{
    class SpecificNameManufacturer
    {
        public static string GetSpecifigManufacturerNameLower(string name)
        {
            var lowerName = name.ToLower();
            switch (lowerName)
            {
                case "r and m":
                    return "r_and_m";
                case "nortel networks":
                    return "nortel_networks";
                case "a-lan tec":
                    return "a-lan tec";
                case "harris stratex":
                    return "harris_stratex";
                case "c and c":
                    return "cc";
                case "alcatel-lucent":
                    return "alcatel-lucent";
                case "hewlett-packard":
                case "hewlett packard":
                case "hewlet packard":
                    return "hp";
                default:
                    return name.ToLower().Trim().Replace(" ", "").Replace("-", "");
            }
        }

        public static string GetSpecificManufacturerNameForIcons(string name)
        {
            var lowerName = name.ToLower();
            switch (lowerName)
            {
                case "alcatel-lucent":
                    return "ALCATEL-LUCENT";
                default:
                    return GetSpecifigManufacturerNameLower(name);
            }
        }

        public static string GetSpecificManufacturerNameUpper(string name)
        {
            var lowerName = name.ToLower();
            switch (lowerName)
            {
                case "r and m":
                    return "R_AND_M";
                case "nortel networks":
                    return "NORTEL_NETWORKS";
                case "harris stratex":
                    return "HARRIS_STRATEX";
                case "a-lan tec":
                    return "A-LAN_TEC";
                case "c and c":
                    return "CC";
                case "alcatel-lucent":
                    return "ALCATEL-LUCENT";
                case "hewlett-packard":
                case "hewlett packard":
                case "hewlet packard":
                    return "HP";
                default:
                    return name.ToUpper().Trim().Replace(" ", "").Replace("-", "");
            }
        }
    }
}
