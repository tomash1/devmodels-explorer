﻿using DevmodelsExplorer.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DevmodelsExplorer.Tools
{
    class ManufacturerXmlManager
    {
        
        private static string GetManufacturerXmlPath(string manufacturerName, string dmlPath)
        {
            return dmlPath + "\\" + SpecificNameManufacturer.GetSpecificManufacturerNameUpper(manufacturerName) + ".inc";
        }

        public static bool AddNewModel(CreatedModel m, string dmlPath)
        {
            var manufFilePath = GetManufacturerXmlPath(m.Manufacturer, dmlPath);
            if (!File.Exists(manufFilePath))
                return false;

            WriteNewModelToManufacturerXml(m, manufFilePath);
            return true;
        }

        private static void WriteNewModelToManufacturerXml(CreatedModel m, string manufFilePath)
        {
            var manufXml = XDocument.Load(manufFilePath);
            var templates = manufXml.Descendants().Where(p => p.Name.ToString().Contains("template"));

            var path = SpecificNameManufacturer.GetSpecifigManufacturerNameLower(m.Manufacturer) + "/" + m.FileName;
            foreach (var template in templates)
            {
                template.Add(new XElement("xsl_REPLACE_ME_TO_COLON_copy-of", new XAttribute("select", "document('" + path + "')/*")));    
            }
            manufXml.Save(manufFilePath);

            var content = File.ReadAllText(manufFilePath);
            var replaced = content.Replace("_REPLACE_ME_TO_COLON_", ":");
            File.WriteAllText(manufFilePath, replaced);
        }
    }
}
