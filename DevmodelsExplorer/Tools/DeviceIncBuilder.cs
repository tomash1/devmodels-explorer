﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using DevmodelsExplorer.Annotations;
using DevmodelsExplorer.Model;
using Microsoft.Win32.SafeHandles;
using NewModelDataType = System.Collections.Generic.Dictionary<string, object>;
using C = DevmodelsExplorer.Model.DeviceClassifiers;

namespace DevmodelsExplorer.Tools
{
    internal class DeviceIncConsts
    {
        public const string DeviceMapModel = "builtin::Modular Device Versions Map Model";

        public static List<string> DeviceExtends = new List<string>()
        { C.BaseDevice };

        public static List<string> DeviceImplements = new List<string>()
        { C.TechnicalDevice, C.NetworkDevice, C.PowerConsumer, C.ModularDevice };
    }

    public class DeviceIncBuilder
    {
        private readonly NewModelDataType _modelData;
        private XmlDocument _deviceXml;
        private XmlNode _deviceModelRoot = null;

        private Dictionary<string, DeviceAttribute> _deviceAttributesMapping = new Dictionary<string, DeviceAttribute>()
        {
            { "width", DeviceAttributes.Width },
            { "height", DeviceAttributes.Height },
            { "depth", DeviceAttributes.Depth },
            { "weight", DeviceAttributes.Weight },
            { "model", DeviceAttributes.Model },
            { "manufacturer", DeviceAttributes.Manufacturer },
            { "ratedCurrent", DeviceAttributes.RatedCurrent },
            { "powerRated", DeviceAttributes.PowerRated },
            { "heatRated", DeviceAttributes.HeatRated }
        };

        public DeviceIncBuilder(NewModelDataType modelData)
        {
            _modelData = modelData;
            _deviceXml = new XmlDocument();
        }

        private XmlNode CreateElement(string withName)
        {
            return _deviceXml.CreateElement(withName);
        }

        private XmlAttribute CreateAttribute(string withName)
        {
            return _deviceXml.CreateAttribute(withName);
        }

        private void DeviceHeader()
        {
            _deviceModelRoot = CreateElement("device-model");

            var mapAttribute = CreateAttribute("autoLocationMapModel");
            mapAttribute.Value = DeviceIncConsts.DeviceMapModel;
            _deviceModelRoot.Attributes?.Append(mapAttribute);

            var nameAttribute = CreateAttribute("name");
            nameAttribute.Value = (string) _modelData["model"];
            _deviceModelRoot.Attributes?.Append(nameAttribute);
        }

        private void DeviceExtends()
        {
            foreach (var deviceExtend in DeviceIncConsts.DeviceExtends)
            {
                var extends = CreateElement("extends");
                extends.InnerText = deviceExtend;
                _deviceModelRoot.AppendChild(extends);
            }
        }

        private void DeviceImplements()
        {
            var deviceImplements = new List<string>();
            deviceImplements.AddRange(DeviceIncConsts.DeviceImplements);
            if ((bool) _modelData["rackMounted"]) deviceImplements.Add(DeviceClassifiers.RackMounted);
            if ((bool) _modelData["expansionModule"]) deviceImplements.Add(DeviceClassifiers.ExpansionModule);

            foreach (var deviceImplement in deviceImplements)
            {
                var implements = CreateElement("implements");
                implements.InnerText = deviceImplement;
                _deviceModelRoot.AppendChild(implements);
            }
        }

        private void DeviceBasic()
        {
            var basicAttributesKeys = new List<string>()
            {
                "width",
                "height",
                "depth",
                "weight",
                "model",
                "manufacturer",
                "ratedCurrent",
                "powerRated",
                "heatRated"
            };

            foreach (var basicAttributesKey in basicAttributesKeys)
            {
                if (_modelData.ContainsKey(basicAttributesKey))
                    AppendDeviceAttribute(basicAttributesKey);
            }
        }

        private string GetAttributeValueModelValue(string modelValue, AttributeType type)
        {
            switch (type)
            {
                case AttributeType.Int:
                    return @"<i>" + modelValue + @"</i>";
                case AttributeType.Real:
                    return @"<r>" + modelValue + @"</r>";
                case AttributeType.String:
                    return @"<s>" + modelValue + @"</s>";
                case AttributeType.Enum:
                    throw new NotImplementedException();
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        private void AppendDeviceAttribute([NotNull] string deviceAttributeKey)
        {
            var deviceAttribute = _deviceAttributesMapping[deviceAttributeKey];

            var deviceClassifier = CreateAttribute("classifier");
            deviceClassifier.Value = deviceAttribute.Classifier;
            var deviceType = CreateAttribute("type");
            deviceType.Value = deviceAttribute.Attribute;

            var deviceAttributeValue = CreateElement("attribute-value");
            deviceAttributeValue.Attributes?.Append(deviceClassifier);
            deviceAttributeValue.Attributes?.Append(deviceType);
            deviceAttributeValue.InnerText = GetAttributeValueModelValue((string)_modelData[deviceAttributeKey], deviceAttribute.Type);

            _deviceModelRoot.AppendChild(deviceAttributeValue);
        }

        public CreatedModel Build()
        {
            DeviceHeader();
            DeviceExtends();
            DeviceImplements();

            DeviceBasic();

            SaveFile();
            return null;
        }

        private string PrepareNewModelFileName()
        {
            var fileName = (string)_modelData["manufacturer"] + "_" + (string)_modelData["model"] + ".inc";
            return fileName.Replace(" ", "_");
        }

        private void SaveFile()
        {
            _deviceXml.AppendChild(_deviceModelRoot);
            var fileName = PrepareNewModelFileName();

            var xmlWriterSettings = new XmlWriterSettings
            {
                ConformanceLevel = ConformanceLevel.Document,
                Indent = true
            };

            using (var stream = File.Create(fileName))
            {
                using (var writer = XmlWriter.Create(stream, xmlWriterSettings))
                {
                    _deviceXml.Save(writer);
                }
            }
            
        }
    }
}
