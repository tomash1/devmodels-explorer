﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevmodelsExplorer.Model;
using SharpSvn;

namespace DevmodelsExplorer.Tools
{
    class CmdExecutor
    {
        private readonly SvnManager _svnManager = null;

        public CmdExecutor(){}

        public CmdExecutor(SvnManager svnManager)
        {
            _svnManager = svnManager;
        }

        public static int ExecuteHidden(string cmd)
        {
            var startInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                FileName = "cmd.exe",
                Arguments = cmd
            };
            var p = new Process { StartInfo = startInfo };
            p.Start();
            p.WaitForExit(int.MaxValue);
            return p.ExitCode;
        }

        private void CreateDirectoryIfNotExists(string directory)
        {
            if (Directory.Exists(directory)) return;

            var createDirCommand = "/C md " + directory + " 2> nul";
            var creationResult = ExecuteHidden(createDirCommand);
            if (creationResult == 0)
                _svnManager.AddFolderToSvn(directory, SvnDepth.Empty);
        }

        public string MoveFileToDirectory(string fileName, string toDirectoryPath, string fromDirectoryPath=null, bool createIfNotExists=true)
        {
            CreateDirectoryIfNotExists(toDirectoryPath);

            var fromPath = (fromDirectoryPath ?? "") + fileName;
            fromPath = "\"" + fromPath + "\"";
            var toPath = toDirectoryPath + "\\" + fileName;
            var returnPath = toPath;
            toPath = "\"" + toPath + "\"";

            var cmd = "/C move " + fromPath + " " + toPath;
            var result = ExecuteHidden(cmd);
            return result == 0 ? returnPath : null;
        }

        public int CopyModelImagesToBlobDirectory(CreatedModel createdModel, string toDirectoryPath)
        {
            CreateDirectoryIfNotExists(toDirectoryPath);

            if (createdModel.IsFrontIconSet())
            {
                var fileToCopy = "\"" + createdModel.FrontIconPath + "\"";
                var toPath = "\"" + toDirectoryPath + "\\" + createdModel.FrontIconFileName + "\"";
                var copyFront = "/C copy " + fileToCopy + " " + toPath;
                var frontExitCode = ExecuteHidden(copyFront);

                if (frontExitCode != 0)
                    return frontExitCode;
                createdModel.FrontIconPath = toDirectoryPath + "\\" + createdModel.FrontIconFileName;
            }

            if (createdModel.IsBackIconSet())
            {
                var fileToCopy = "\"" + createdModel.BackIconPath + "\"";
                var toPath = "\"" + toDirectoryPath + "\\" + createdModel.BackIconFileName + "\"";
                var copyBack = "/C copy " + fileToCopy + " " + toPath;
                               
                var backExitCode = ExecuteHidden(copyBack);
                if (backExitCode == 0)
                    createdModel.BackIconPath = toDirectoryPath + "\\" + createdModel.BackIconFileName;
                return backExitCode;
            }
            return 0;
        }

        public static void SelectPathInWindowsExplorer(string path)
        {
            if (path != null)
                Process.Start("explorer.exe", "/select," + path);
        }
    }
}
