﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Media.Imaging;
using DevmodelsExplorer.Model;

namespace DevmodelsExplorer.Tools
{
    internal class ImageManager
    {
        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool DeleteObject(IntPtr value);

        public static BitmapImage CreateBitmap(UserImage userImage)
        {
            try
            {
                using (var fs = new FileStream(userImage.Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    var image = new BitmapImage();
                    image.BeginInit();
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.StreamSource = fs;
                    image.EndInit();
                    image.Freeze();
                    return image;
                }
            }
            catch (Exception) {}
            return null;
        }

        public static Dictionary<string, int> ResolveImageHeightAndWidthInPx(string filePath)
        {
            var width = 0;
            var height = 0;
            if (filePath != null)
            {
                var im = Image.FromFile(filePath);
                width = im.Width;
                height = im.Height;
            }
            var result = new Dictionary<string, int>
            {
                ["Width"] = width,
                ["Height"] = height
            };
            return result;
        }
    }
}
 