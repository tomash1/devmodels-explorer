﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Linq;
using DevmodelsExplorer.Annotations;
using DevmodelsExplorer.Model;

namespace DevmodelsExplorer.Tools
{
    public class DeviceManager
    {
        #region Fields

        private List<Device> _devices;

        private string _modelQuery = "";
        private string _manufQuery = "";
        private IEnumerable<string> _incs = null;

        private Dispatcher _dispatcher;

        #endregion

        #region Properties

        public static int CallbackCallFrequency = 10;

        public int IncsCount => _incs?.Count() ?? 0;

        public IEnumerable<Device> Devices => _devices;
        public string DmlPath { get; private set; } = null;

        public IEnumerable<string> Manufacturers
        {
            get
            {
                var m = new List<string>();
                var addedNormalizedNames = new List<string>();
                foreach (var d in Devices)
                {
                    var manufName = d.Manufacturer;
                    var manufNameNormalized = manufName.First().ToString().ToUpper() + manufName.Substring(1).ToLower();
                    if (addedNormalizedNames.Contains(manufNameNormalized)) continue;

                    addedNormalizedNames.Add(manufNameNormalized);
                    m.Add(d.Manufacturer);
                }
                return m;
            }
        }

        #endregion

        #region Constructor

        public DeviceManager(Dispatcher dispatcher)
        {
            _devices = new List<Device>();
            _dispatcher = dispatcher;
        }

        #endregion

        #region Filter Devices

        private IEnumerable<Device> GetFiltered()
        {
            return _devices
                .Where(d => d.Manufacturer.ToLower().StartsWith(_manufQuery))
                .Where(d => d.Name.ToLower().Contains(_modelQuery))
                .ToList();
        }

        public IEnumerable<Device> FilterDevicesByModel(string q)
        {
            _modelQuery = q ?? "";
            return GetFiltered();
        }

        public IEnumerable<Device> FilterDeviceByManufacturer(string q)
        {
            _manufQuery = q ?? "";
            return GetFiltered();
        }

        # endregion

        public void FindIncsInPath(string path)
        {
            DmlPath = path;
            try
            {
                _incs = Directory.GetFiles(path, "*.inc", SearchOption.AllDirectories);
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
                _incs = new string[] { };
            }
        }

        #region Xml Parsing

        private static string GetSingleValueAttribute(XContainer attributeValue)
        {
            var elements = attributeValue.Descendants();
            var element = elements.FirstOrDefault();
            string singleValue = null;
            if (element != null)
                singleValue = element.Value;
            return singleValue;
        }

        private static Dictionary<string, string> GetBlobAttribute(XContainer attributeValue)
        {
            var elements = attributeValue.Descendants();
            var element = elements.FirstOrDefault();
            if (element == null || !element.HasAttributes) return null;
            var path = element.Attribute("path").Value;
            var name = element.Attribute("name").Value;
            var dict = new Dictionary<string, string>
            {
                {"PATH", path},
                { "NAME", name}
            };
            return dict;
        }

        public Device ParseDevice(string path)
        {
            var xdoc = XDocument.Load(path);

            var lv1s = from lv1 in xdoc.Descendants("device-model")
                       select new
                       {
                           ModelName = lv1.Attribute("name").Value,
                           Extends = lv1.Descendants("extends"),
                           Implements = lv1.Descendants("implements"),
                           AttributeValues = lv1.Descendants("attribute-value")
                       };

            if (!lv1s.Any()) return null;

            var d = new Device { FilePath = path };
            foreach (var lv1 in lv1s)
            {
                foreach (var extends in lv1.Extends)
                    d.AddExtends(extends.Value);
                foreach (var implements in lv1.Implements)
                    d.AddImplements(implements.Value);

                foreach (var attributeValue in lv1.AttributeValues)
                    if (attributeValue.Attribute("classifier").Value == "::Branded Item" && attributeValue.Attribute("type").Value == "Model")
                        d.Name = GetSingleValueAttribute(attributeValue);
                    else if (attributeValue.Attribute("classifier").Value == "::Branded Item" && attributeValue.Attribute("type").Value == "Manufacturer")
                        d.Manufacturer = GetSingleValueAttribute(attributeValue);
                    else if (attributeValue.Attribute("classifier").Value == "::3D Representation" && attributeValue.Attribute("type").Value == "FrontIcon")
                        d.Add3DFrontIcon(GetBlobAttribute(attributeValue));
                    else if (attributeValue.Attribute("classifier").Value == "::3D Representation" && attributeValue.Attribute("type").Value == "BackIcon")
                        d.Add3DBackIcon(GetBlobAttribute(attributeValue));
                    else if (attributeValue.Attribute("classifier").Value == "::2D Representation" && attributeValue.Attribute("type").Value == "Icon")
                        d.Add2DIcon(GetBlobAttribute(attributeValue));
            }
            return d;
        }

        #endregion

        public void AddDevice([NotNull] Device d)
        {
            _devices.Add(d);
        }

        public bool AreDevicesFound()
        {
            return _devices.Count != 0;
        }

        private void ParseDevices(Action<string> callback)
        {
            _devices = new List<Device>();
            var counter = 0;
            foreach (var file in _incs)
            {
                var d = ParseDevice(file);
                if (d == null) continue;

                if (callback != null)
                {
                    counter = (counter + 1) % CallbackCallFrequency;
                    if (counter == 0)
                        _dispatcher?.Invoke(() => callback(d.Name));
                }
                _devices.Add(d);
            }

            if (callback != null)
                _dispatcher?.Invoke(() => callback(null));
        }

        public void ReadDevicesFromFoundIncs(Action<string> nextDeviceCallback)
        {
            new Thread(
                () => ParseDevices(nextDeviceCallback)
                )
                .Start();
        }
    }
}
