﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevmodelsExplorer.Model;
using Newtonsoft.Json;

namespace DevmodelsExplorer.Tools
{
    class PythonExecutor
    {
        private static string ConvertModelDataFromDictionaryToXmlWriterStringParam(IDictionary<string, object> modelData)
        {
            var emptyKeys = new List<string>();
            var spaceKeys = new List<string>();
            foreach (var keyValuePair in modelData)
            {
                if (keyValuePair.Value == null)
                    emptyKeys.Add(keyValuePair.Key);
                else if (keyValuePair.Value.GetType() == "".GetType())
                {
                    var val = (string) keyValuePair.Value;
                    if (val.Contains(" "))
                        spaceKeys.Add(keyValuePair.Key);
                }
            }
            foreach (var emptyKey in emptyKeys)            
                modelData[emptyKey] = "";
            foreach (var spaceKey in spaceKeys)
                modelData[spaceKey] = ((string)modelData[spaceKey]).Replace(" ", "_SnPbAsCpE;_");
            return JsonConvert.SerializeObject(modelData).Replace("\"", "'");
        }

        private static string PrepareNewModelFileName(object manufacturer, object model)
        {
            var fileName = (string) manufacturer + "_" + (string) model + ".inc";
            return fileName.Replace(" ", "_");
        }

        public CreatedModel Create(Dictionary<string, object> modelData)
        {
            var manufacturerName = (string) modelData["manufacturer"];
            var fileName = PrepareNewModelFileName(modelData["manufacturer"], modelData["model"]);

            var pythonArgModelData = ConvertModelDataFromDictionaryToXmlWriterStringParam(modelData);
            var cmd = "/C python.exe Assets\\XmlWriter.py " + fileName + " " + pythonArgModelData;

            var exitCode = CmdExecutor.ExecuteHidden(cmd);
            return exitCode != 0 ? null : new CreatedModel()
            {
                FileName = fileName,
                Manufacturer = manufacturerName
            };
        }

    }
}
