﻿using System;
using System.Threading;
using System.Windows.Threading;
using DevmodelsExplorer.Properties;
using SharpSvn;

namespace DevmodelsExplorer.Tools
{
    internal class SvnManager
    {
        public enum SvnResult { Ok, Old, CannotGetLocalRepo, SvnNotAvailable}

        private readonly Dispatcher _dispatcher = null;
        private readonly string _path = null;
        private readonly bool _addToSvn;

        #region Constructor

        public SvnManager(){}

        public SvnManager(bool addToSvn)
        {
            _dispatcher = null;
            _path = null;
            _addToSvn = addToSvn;
        }

        public SvnManager(Dispatcher viewDispatcher, string repoPath, bool addToSvn=true)
        {
            _dispatcher = viewDispatcher;
            _path = repoPath;
            _addToSvn = addToSvn;
        }

        #endregion

        private long GetLocalWorkingDirectoryRevision()
        {
            using (var client = new SvnWorkingCopyClient())
            {
                SvnWorkingCopyVersion version;
                client.GetVersion(_path, out version);
                return version.Start;
            }
        }

        private Uri GetLocalRepositoryUri()
        {
            using (var client = new SvnClient())
            {
                SvnInfoEventArgs info;
                client.GetInfo(_path, out info);
                return info.Uri;
            }
        }

        private static long GetRemoteRepositoryHeadRevision(Uri remotePath)
        {
            using (var client = new SvnClient())
            {
                SvnInfoEventArgs info;
                client.GetInfo(remotePath, out info);
                return info.LastChangeRevision;
            }
        }

        private void CheckData(Action<SvnResult> resultCallback)
        {
            var result = SvnResult.CannotGetLocalRepo;
            try
            {
                var localRevision = GetLocalWorkingDirectoryRevision();
                var localRepoUri = GetLocalRepositoryUri();
                var remoteRevision = GetRemoteRepositoryHeadRevision(localRepoUri);
                
                result = localRevision >= remoteRevision ? SvnResult.Ok : SvnResult.Old;
            }
            catch (Exception exception)
            {
                if (exception.Message.Contains("Unable to connect"))
                    result = SvnResult.SvnNotAvailable;
            }
            _dispatcher.Invoke(() => resultCallback(result));
        }

        public void IsNewDataAvailable(Action<SvnResult> resultCallback)
        {
            if (_path == null)
                throw new ArgumentNullException("_path", Resources.SvnManager_path_is_not_set_Exception_Message);
            if (_dispatcher == null)
                throw new ArgumentNullException("_dispatcher", Resources.SvnManager_dispatcher_is_not_set);
            new Thread(
                () => CheckData(resultCallback)
                )
                .Start();
        }

        public bool AddFileToSvn(string filePath)
        {
            if (!_addToSvn) return true;
            try
            {
                using (var client = new SvnClient())
                {
                    return client.Add(filePath);
                }
            }
            catch (SvnEntryException) { }
            return false;
        }

        public bool AddFolderToSvn(string folderPath, SvnDepth depth)
        {
            if (!_addToSvn) return true;
            try
            {
                using (var client = new SvnClient())
                {
                    return client.Add(folderPath, depth);
                }
            }
            catch(SvnEntryException) { }
            return false;
        }

        private void UpdateSvn(Action<bool> resultCallback)
        {
            try
            {
                using (var client = new SvnClient())
                {
                    var result = client.Update(_path);
                    _dispatcher.Invoke(() => resultCallback(result));
                }
            }
            catch (Exception)
            {
                _dispatcher.Invoke(() => resultCallback(false));
            }
        }

        public void UpdateSvnRoot(Action<bool> resultCallback)
        {
            if (_path == null)
                throw new ArgumentNullException("_path", Resources.SvnManager_path_is_not_set_Exception_Message);
            if (_dispatcher == null)
                throw new ArgumentNullException("_dispatcher", Resources.SvnManager_dispatcher_is_not_set);
            new Thread(
                () => UpdateSvn(resultCallback)
                )
                .Start();
        }
    }
}
