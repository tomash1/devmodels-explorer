import json
import sys

__author__ = 'rweber'

import re
import uuid
import xml.etree.ElementTree as ETree
from xml.dom import minidom
import traceback


def random_GUID_generator():
    output_string = str(uuid.uuid4())
    return output_string


def prettify(element):
    """
    Return a pretty-printed XML string for the Element.
    """
    rough_string = ETree.tostring(element, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="    ")


def delete_xml_version_declaration(xml_file):
    with open(xml_file, 'r') as file_in:
        data = file_in.read().splitlines(True)
        if len(data) > 0:
            first_line = data[0]
            if check_for_xml_version_declaration(first_line):
                with open(xml_file, 'w') as file_out:
                    file_out.writelines(data[1:])


def check_for_xml_version_declaration(first_line):
    if re.search('<\?xml version=', first_line) is not None:
        return True
    else:
        return False


class XmlWriter():
    def __init__(self, output_file_path=''):
        self.xml_output_file_path = output_file_path
        self.xml_tree = ETree.ElementTree()
        self.xml_device = None
        self.portsDefaultValues = {}
        self.deviceAttributesTemplates = {}
        self.deviceExtends = []
        self.deviceImplements = []
        self.initTemplates()

    def initTemplates(self):
        self.deviceExtends = ['::Base Device']

        self.deviceImplements = ['::Technical Device',
                                 '::Network Device',
                                 '::Power Consumer']

        self.defineDeviceAttributesTemplates(codeAttributeName='depth', classifier='::2-Dimensioned Item', type='Depth', attributeType='i')
        self.defineDeviceAttributesTemplates(codeAttributeName='width', classifier='::2-Dimensioned Item', type='Width', attributeType='i')
        self.defineDeviceAttributesTemplates(codeAttributeName='height', classifier='::3-Dimensioned Item', type='Height', attributeType='i')
        self.defineDeviceAttributesTemplates(codeAttributeName='heightInScrews', classifier='::Rack Mounted', type='HeightInScrews', attributeType='i')
        self.defineDeviceAttributesTemplates(codeAttributeName='weight', classifier='::Weighted Item', type='Weight', attributeType='r')
        self.defineDeviceAttributesTemplates(codeAttributeName='model', classifier='::Branded Item', type='Model', attributeType='s')
        self.defineDeviceAttributesTemplates(codeAttributeName='manufacturer', classifier='::Branded Item', type='Manufacturer', attributeType='s')
        self.defineDeviceAttributesTemplates(codeAttributeName='ratedCurrent', classifier='::Power Consumer', type='RatedCurrent', attributeType='r')
        self.defineDeviceAttributesTemplates(codeAttributeName='powerRated', classifier='::Power Consumer', type='Power Rated', attributeType='r')
        self.defineDeviceAttributesTemplates(codeAttributeName='heatRated', classifier='::Power Consumer', type='Heat Rated', attributeType='r')

        self.defineDeviceAttributesTemplates(codeAttributeName='slotX', classifier='::Technical Device Expansion Module', type='SlotX', attributeType='i')
        self.defineDeviceAttributesTemplates(codeAttributeName='slotY', classifier='::Technical Device Expansion Module', type='SlotY', attributeType='i')
        self.defineDeviceAttributesTemplates(codeAttributeName='canRotate', classifier='::Technical Device Expansion Module', type='CanRotate', attributeType='i')

        self.defineDeviceAttributesTemplates(codeAttributeName='status', classifier='::Generic Port', type='Status', attributeType='e')
        self.defineDeviceAttributesTemplates(codeAttributeName='name', classifier='::Generic Port', type='Name', attributeType='s')
        self.defineDeviceAttributesTemplates(codeAttributeName='speed', classifier='::Network Port', type='Speed', attributeType='e')
        self.defineDeviceAttributesTemplates(codeAttributeName='type', classifier='::Network Port', type='Type', attributeType='e')
        self.defineDeviceAttributesTemplates(codeAttributeName='locationInDevice', classifier='::Network Port', type='LocationInDevice', attributeType='i')
        self.defineDeviceAttributesTemplates(codeAttributeName='parentId', classifier='::Network Port', type='ParentID', attributeType='i')

        self.definePortsTemplates(portCode='C14', model='objmodels::IEC 60320 C14', name='IEC 60320 C14', direction='IN', status='Valid')
        self.definePortsTemplates(portCode='C13', model='objmodels::IEC 60320 C13', name='IEC 60320 C13', direction='OUT', status='Valid')
        self.definePortsTemplates(portCode='C19', model='objmodels::IEC 60320 C19', name='IEC 60320 C19', direction='IN', status='Valid')
        self.definePortsTemplates(portCode='C20', model='objmodels::IEC 60320 C20', name='IEC 60320 C20', direction='OUT', status='Valid')
        self.definePortsTemplates(portCode='L6-30P', model='objmodels::NEMA L6-30P', name='NEMA L6-30P', direction='IN', status='Valid')
        self.definePortsTemplates(portCode='L6-30R', model='objmodels::NEMA L6-30R', name='NEMA L6-30R', direction='IN', status='Valid')

        self.definePortsTemplates(portCode='copper',
                                  model='objmodels::Ethernet Port CM',
                                  name='Eth',
                                  direction='INOUT',
                                  status='Valid',
                                  type='Copper')

        self.definePortsTemplates(portCode='optical',
                                  model='objmodels::Fibre Channel Port CM',
                                  name='Fibre Channel Port',
                                  direction='INOUT',
                                  status='Valid',
                                  type='Fiber')

    def defineDeviceAttributesTemplates(self, codeAttributeName, classifier, type, attributeType):
        self.deviceAttributesTemplates[codeAttributeName] = {'classifier'   : classifier,
                                                             'type'         : type,
                                                             'attributeType': attributeType}

    def definePortsTemplates(self,
                             portCode,
                             model,
                             name,
                             direction,
                             status,
                             type=None,
                             speed='Unspecified'):
        self.portsDefaultValues[portCode] = {'model'    : model,
                                             'name'     : name,  # tutaj model
                                             'direction': direction,
                                             'status'   : status,
                                             'type'     : type,
                                             'speed'    : speed}

    def CreateModel(self, deviceAttributesFromForm={}):
        self.writeDeviceHeader(deviceAttributesFromForm['model'], deviceAttributesFromForm['modularDevice'])
        self.writeExtends()
        self.writeImplements(deviceAttributesFromForm)

        self.writeDeviceBasicAttributes(deviceAttributesFromForm)
        self.writeDeviceAdditionalAttributes(deviceAttributesFromForm)

        self.writeDeviceVisualAttributes(deviceAttributesFromForm)
        self.writeModularDeviceAttributes(deviceAttributesFromForm)

        locationMap = self.writeLocationMapForPorts()
        self.writeDevicePowerPorts(locationMap, deviceAttributesFromForm)
        self.writeDeviceNetworkPorts(locationMap, deviceAttributesFromForm)

        with open(self.xml_output_file_path, 'w') as xml_file:
            xml_file.write(prettify(self.xml_device))

        delete_xml_version_declaration(self.xml_output_file_path)

    def writeDeviceHeader(self, deviceName, isModularDevice):
        mapModel = 'builtin::Modular Device Versions Map Model'
        self.xml_device = ETree.Element('device-model', {'name'                : deviceName,
                                                         'autoLocationMapModel': mapModel})

    def writeExtends(self):
        device_extends = ETree.SubElement(self.xml_device,
                                          'extends')
        for value in self.deviceExtends:
            device_extends.text = value

    def writeImplements(self, deviceAttributesFromForm):
        categories = ['::' + deviceAttributesFromForm['category']]
        categories.extend(self.deviceImplements)

        if deviceAttributesFromForm['rackMounted']:
            categories.append('::Rack Mounted')

        categories.append('::Modular Device')

        if deviceAttributesFromForm['expansionModule']:
            categories.append('::Technical Device Expansion Module')

        for category in categories:
            category_tag = ETree.SubElement(self.xml_device,
                                            'implements')
            category_tag.text = category

    #######################################  DEVICE BASIC ATTRIBUTES  ########################################

    def writeDeviceBasicAttributes(self, deviceAttributesData):
        basicAttributes = ['width',
                           'height',
                           'depth',
                           'weight',
                           'model',
                           'manufacturer',
                           'ratedCurrent',
                           'powerRated',
                           'heatRated']

        for attributeIndex in basicAttributes:
            if attributeIndex in deviceAttributesData:
                self.writeDeviceAttribute(deviceAttributesData, attributeIndex)

    def writeDeviceAdditionalAttributes(self, deviceAttributesData):
        if deviceAttributesData['rackMounted']:
            self.writeDeviceAttribute(deviceAttributesData, 'heightInScrews')

        if deviceAttributesData['expansionModule']:
            self.writeDeviceAttribute(deviceAttributesData, 'slotX')
            self.writeDeviceAttribute(deviceAttributesData, 'slotY')
            self.writeDeviceAttribute(deviceAttributesData, 'canRotate', '0')

    def writeDeviceAttribute(self, deviceAttributesData, attributeIndex, valueFromArgs=None):
        deviceAttributesTemplate = self.deviceAttributesTemplates[attributeIndex]
        attributeValueTag = ETree.SubElement(self.xml_device, 'attribute-value',
                                             attrib={'classifier': deviceAttributesTemplate['classifier'],
                                                     'type'      : deviceAttributesTemplate['type']})
        value = ETree.SubElement(attributeValueTag, deviceAttributesTemplate['attributeType'])
        if valueFromArgs is None:
            value.text = str(deviceAttributesData[attributeIndex])
        else:
            value.text = str(valueFromArgs)

    #######################################  VISUAL ATTRIBUTES  ########################################

    def writeDeviceVisualAttributes(self, deviceAttributesFromForm):
        imageFrontFilePath = deviceAttributesFromForm['imageFrontFilePath']
        imageBackFilePath = deviceAttributesFromForm['imageBackFilePath']
        imageFrontWidth = deviceAttributesFromForm['imageFrontWidth']
        imageFrontHeight = deviceAttributesFromForm['imageFrontHeight']
        imageBackWidth = deviceAttributesFromForm['imageBackWidth']
        imageBackHeight = deviceAttributesFromForm['imageBackHeight']

        manufacturer = deviceAttributesFromForm['manufacturer']
        manufacturer = manufacturer.lower()
        manufacturer = manufacturer.replace(' ', '_')
        if manufacturer is 'hewlett-packard':
            manufacturer = 'hp'

        visualAttributes = {'Icon'          : [''],
                            'IconWidthInMM' : [250],
                            'IconHeightInMM': [250],
                            'XScaling'      : [1],
                            'YScaling'      : [1]}
        attributesPrefixes = ['', 'Front', 'Back']

        for prefix in attributesPrefixes:
            for attribute in visualAttributes:
                if prefix == '':
                    classifier = '::2D Representation'
                else:
                    classifier = '::3D Representation'

                if attribute == 'Icon' and prefix == '':
                    attributeType = 'b name=\"dml_item.png\" path=\"att.blobs/dml_item.png\"'
                elif attribute == 'Icon' and prefix == 'Front' and len(imageFrontFilePath) != 0:
                    attributeType = 'b name=\"' + imageFrontFilePath + '\" path="att.blobs/manufacturer/' + manufacturer + '/' + imageFrontFilePath + '"'
                elif attribute == 'Icon' and prefix == 'Front' and len(imageFrontFilePath) == 0:
                    attributeType = 'NULL'
                elif attribute == 'Icon' and prefix == 'Back' and len(imageBackFilePath) != 0:
                    attributeType = 'b name=\"' + imageFrontFilePath + '\" path="att.blobs/manufacturer/' + manufacturer + '/' + imageBackFilePath + '"'
                elif attribute == 'Icon' and prefix == 'Back' and len(imageBackFilePath) == 0:
                    attributeType = 'NULL'
                else:
                    attributeType = 'i'

                attributeValueTag = ETree.SubElement(self.xml_device,
                                                     'attribute-value',
                                                     attrib={'classifier': classifier,
                                                             'type'      : prefix + str(attribute)})

                for attr in visualAttributes[attribute]:
                    value = ETree.SubElement(attributeValueTag, attributeType)

                    if prefix == 'Back' and len(imageBackFilePath) != 0:
                        visualAttributes['IconWidthInMM'] = self.getIconDimensionAttribute(imageBackWidth)
                        visualAttributes['IconHeightInMM'] = self.getIconDimensionAttribute(imageBackHeight)
                        value.text = str(attr)
                    elif prefix == 'Front' and len(imageFrontFilePath) != 0:
                        visualAttributes['IconWidthInMM'] = self.getIconDimensionAttribute(imageFrontWidth)
                        visualAttributes['IconHeightInMM'] = self.getIconDimensionAttribute(imageFrontHeight)
                        value.text = str(attr)
                    elif attributeType == 'i' or attributeType == 'NULL' or attributeType == 'b name=\"dml_item.png\" path=\"att.blobs/dml_item.png\"':
                        visualAttributes['IconWidthInMM'] = [250]
                        visualAttributes['IconHeightInMM'] = [250]
                        value.text = str(attr)
                    else:
                        value.text = str(attr)

    def getIconDimensionAttribute(self, imageDimension):
        if len(imageDimension) != 0:
            return [str(imageDimension)]
        else:
            return [str(250)]

    #######################################  MODULAR SLOTS  ############################################

    def writeModularDeviceAttributes(self, deviceAttributesFromForm):
        if deviceAttributesFromForm['modularDevice']:
            if deviceAttributesFromForm['modularFrontDevice']:
                self.writeModularDeviceDefaultAttributes('Front')
            if deviceAttributesFromForm['modularBackDevice']:
                self.writeModularDeviceDefaultAttributes('Back')

    def writeModularDeviceDefaultAttributes(self, positionInDevice=''):
        position = positionInDevice.capitalize()

        modularDeviceAttributes = {position + 'Rows'                           : [20, 20],
                                   position + 'Columns'                        : [20, 80],
                                   position + 'ElementsTypes'                  : ['SingleSlot', 'SingleSlot'],
                                   position + 'ElementsNames'                  : ['Default Slot 1', 'Default Slot 2'],
                                   position + 'ElementsGridRowIdxAssigments'   : [0, 1],
                                   position + 'ElementsGridColumnIdxAssigments': [0, 1],
                                   position + 'ElementsGridRowspanValues'      : [1, 1],
                                   position + 'ElementsGridColumnspanValues'   : [1, 1]}

        for modularDeviceAttribute in modularDeviceAttributes.keys():
            attribute_value = ETree.SubElement(self.xml_device,
                                               'attribute-value',
                                               {'classifier': '::Modular Device',
                                                'type'      : modularDeviceAttribute})

            for sub_attribute in modularDeviceAttributes[modularDeviceAttribute]:
                if modularDeviceAttribute == position + 'ElementsTypes':
                    attribute_type = 'e'
                elif modularDeviceAttribute == position + 'ElementsNames':
                    attribute_type = 's'
                else:
                    attribute_type = 'i'

                value = ETree.SubElement(attribute_value, attribute_type)
                value.text = str(sub_attribute)

    #######################################  PORTS  ############################################

    def writeLocationMapForPorts(self):
        locationMap = ETree.SubElement(self.xml_device,
                                       'location-map',
                                       {'model': 'builtin::Ports LMM',
                                        'id'   : random_GUID_generator()})
        return locationMap

    def writeDevicePowerPorts(self, locationMap, deviceAttributesFromForm):
        powerPortsCountIN = int(deviceAttributesFromForm['powerPortsCountIN'])
        powerPortsCountOUT = int(deviceAttributesFromForm['powerPortsCountOUT'])

        if powerPortsCountIN is not None:
            self.writeDevicePowerPort(locationMap, deviceAttributesFromForm['powerPortsTypeIN'], powerPortsCountIN)

        if powerPortsCountOUT is not None:
            self.writeDevicePowerPort(locationMap, deviceAttributesFromForm['powerPortsTypeOUT'], powerPortsCountOUT)

    def writeDevicePowerPort(self, locationMap, portType, power_ports_count):
        powerAttributes = ['status', 'name']
        portDefaultsValues = self.portsDefaultValues[portType]

        for port_number in range(power_ports_count):
            item = ETree.SubElement(locationMap, 'item')
            self.writePortsAdditionalInformations(item, 'PowerPort')

            localisableItem = ETree.SubElement(item,
                                               'localisable-item',
                                               {'movability': 'NONE'})

            connector_object = ETree.SubElement(localisableItem,
                                                'connector-object',
                                                {'model'    : portDefaultsValues['model'],
                                                 'direction': portDefaultsValues['direction'],
                                                 'id'       : random_GUID_generator()})

            for attribute in powerAttributes:
                deviceAttributesTemplate = self.deviceAttributesTemplates[attribute]

                attribute_value = ETree.SubElement(connector_object,
                                                   'attribute-value',
                                                   {'type'      : deviceAttributesTemplate['type'],
                                                    'classifier': deviceAttributesTemplate['classifier']})

                attr_value = ETree.SubElement(attribute_value,
                                              deviceAttributesTemplate['attributeType'])
                if attribute == 'name':
                    attr_value.text = str(portDefaultsValues[attribute] + ' %02d' % (port_number + 1,))
                else:
                    attr_value.text = str(portDefaultsValues[attribute])

    def writeDeviceNetworkPorts(self, locationMap, deviceAttributesFromForm):
        self.buildAndWriteNetworkPorts(locationMap,
                                       'copper',
                                       int(deviceAttributesFromForm['networkPortsCountCopper']),
                                       deviceAttributesFromForm['category'])

        self.buildAndWriteNetworkPorts(locationMap,
                                       'optical',
                                       int(deviceAttributesFromForm['networkPortsCountOptical']),
                                       deviceAttributesFromForm['category'])

    def buildAndWriteNetworkPorts(self, locationMap, portType, portsCount, deviceCategory):
        defaultValues = self.portsDefaultValues[portType]

        if deviceCategory.strip() == 'LAN Patch Panel' or deviceCategory.strip() == 'SAN Patch Panel':
            for portSide in ['Front', 'Rear']:
                for portNumber in range(1, portsCount + 1):
                    attributes = {'status'  : defaultValues['status'],
                                  'type'    : defaultValues['type'],
                                  'name'    : defaultValues['name'] + ' {} - {}'.format(portNumber, portSide),
                                  'parentId': int(portNumber)
                                  }

                    self.writeDeviceNetworkPort(locationMap, attributes, self.portsDefaultValues[portType])
        else:
            for portNumber in range(1, portsCount + 1):
                attributes = {'status': defaultValues['status'],
                              'type'  : defaultValues['type'],
                              'name'  : defaultValues['name'] + ' ' + unicode(portNumber)
                              }

                self.writeDeviceNetworkPort(locationMap, attributes, self.portsDefaultValues[portType])

    def writeDeviceNetworkPort(self, locationMap, attributes, portDefaultValues):
        item = ETree.SubElement(locationMap, 'item')
        self.writePortsAdditionalInformations(item, 'NetworkPort')

        localisable_item = ETree.SubElement(item,
                                            'localisable-item',
                                            {'movability': 'NONE'})
        connector_object = ETree.SubElement(localisable_item,
                                            'connector-object',
                                            {'model'    : portDefaultValues['model'],
                                             'direction': portDefaultValues['direction'],
                                             'id'       : random_GUID_generator()})

        for attribute in attributes:
            deviceAttributesTemplate = self.deviceAttributesTemplates[attribute]

            attribute_value = ETree.SubElement(connector_object,
                                               'attribute-value',
                                               {'type'      : deviceAttributesTemplate['type'],
                                                'classifier': deviceAttributesTemplate['classifier']})
            attr_value = ETree.SubElement(attribute_value,
                                          deviceAttributesTemplate['attributeType'])
            attr_value.text = str(attributes[attribute])

    def writePortsAdditionalInformations(self, item, portType):
        attributes = [{'attributeType': 'i',
                       'attributeName': 'X',
                       'attributeValue': '1'},

                      {'attributeType': 'i',
                       'attributeName': 'Y',
                       'attributeValue': '1'},

                      {'attributeType': 'i',
                       'attributeName': 'Rotation',
                       'attributeValue': '0'},

                      {'attributeType': 'e',
                       'attributeName': 'Side',
                       'attributeValue': 'Unspecified'},

                      {'attributeType': 'e',
                       'attributeName': 'Type',
                       'attributeValue': portType}
                      ]

        for values in attributes:
            self.writePortsAdditionalInformation(item, values)

    def writePortsAdditionalInformation(self, item, values):
        attribute = ETree.SubElement(item, values['attributeType'], {'name': values['attributeName']})
        attribute.text = values['attributeValue']


def parseJsonModelData(jsonModelData):
    json_acceptable_string = jsonModelData.replace("'", "\"")
    json_acceptable_string = json_acceptable_string.replace("_SnPbAsCpE;_", " ")
    d = json.loads(json_acceptable_string)
    return d


if __name__ == '__main__':
    filename = sys.argv[1]
    modelData = sys.argv[2]
    try:
        parsedModelData = parseJsonModelData(modelData)
        for k, v in parsedModelData.iteritems():
            if v == 'true':
                parsedModelData[k] = True
            elif v == 'false':
                parsedModelData[k] = False
        XmlWriter(output_file_path=filename).CreateModel(parsedModelData)
        sys.exit(0)

    except Exception, ex:
        with open('error.txt', 'w') as excFile:
            excFile.write(modelData + "\n")
            excFile.write(str(ex) + '\n')
            excFile.write(traceback.format_exc())
        sys.exit(1)
