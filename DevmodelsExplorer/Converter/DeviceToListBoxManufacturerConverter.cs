﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using DevmodelsExplorer.Model;

namespace DevmodelsExplorer.Converter
{
    internal class DeviceToListBoxManufacturerConverter : IValueConverter
    {


        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var device = value as Device;
            return device == null ? "Błąd: null" : device.Manufacturer;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
