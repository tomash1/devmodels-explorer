﻿using System;
using System.Globalization;
using System.Reflection;
using System.Windows.Data;
using DevmodelsExplorer.Model;
using DevmodelsExplorer.Tools;

namespace DevmodelsExplorer.Converter
{
    internal class UserImageToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var photo = value as UserImage;
            if (photo == null)
                return ResourceManager.GetImage("default-image.png");
            var image = ImageManager.CreateBitmap(photo);
            return image != null ? (object) image : ResourceManager.GetImage("default-image.png");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
