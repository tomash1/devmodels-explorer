﻿using System;
using System.Globalization;
using System.Windows.Data;
using DevmodelsExplorer.Model;

namespace DevmodelsExplorer.Converter
{
    internal class DeviceToListBoxModelNameConverter: IValueConverter
    {
        

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var device = value as Device;
            return device == null ? "Błąd: null" : device.Name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
