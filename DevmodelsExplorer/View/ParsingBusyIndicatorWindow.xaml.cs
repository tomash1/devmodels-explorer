﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DevmodelsExplorer.View
{
    /// <summary>
    /// Interaction logic for ParsingBusyIndicatorWindow.xaml
    /// </summary>
    public partial class ParsingBusyIndicatorWindow : Window
    {

        private int _parsed = 0;        

        public int Max { set { ProgressPresenter.Maximum = value; } }

        public ParsingBusyIndicatorWindow()
        {
            InitializeComponent();
        }

        public void Hide()
        {
            Close();
        }

        public void UpdadeProgressBar(string value)
        {
            ++_parsed;
            ProgressPresenter.Value = _parsed;
            AddingDeviceNameLabel.Content = value;
        }


    }
}
