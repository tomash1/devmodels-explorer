﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevmodelsExplorer.ViewModel;

namespace DevmodelsExplorer.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly MainWindowViewModel _viewModel;

        public MainWindow()
        {
            _viewModel = new MainWindowViewModel(this);
            _viewModel.InitAppLook();
            InitializeComponent();
            DataContext = _viewModel;
            Closing += MainWindow_Closing;
            _viewModel.DevicesListBox = ListBox1;
        }

        private static void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _viewModel.ListBoxSelectionChanged();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var isSvnChechEnabled = CheckSvnCheckbox.IsChecked ?? false;
            _viewModel.OpenDevmodelsButton_Click(isSvnChechEnabled);
        }

        private void ModelNameTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            var filter = FilterQueryTextBox.Text;
            if (filter != null)
                _viewModel.FilterDevicesByModelName(filter.ToLower().Trim());
        }

        private void ManufacturerNameTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            var filter = ManufacturerQueryTextBox.Text;
            if (filter != null)
                _viewModel.FilterDevicesByManufacturerName(filter.ToLower().Trim());
        }

        private void listBox1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            _viewModel.ListBoxDoubleClicked();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            _viewModel.Icon2dPathOpen_Click();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            _viewModel.Icon3dFrontPathOpen_Click();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            _viewModel.Icon3dBackPathOpen_Click();
        }

        private void ButtonNewModel_OnClick(object sender, RoutedEventArgs e)
        {
            _viewModel.AddNewModel_Click();
        }

        public void ShowBusyIndicator()
        {
            Overlay.Visibility = Visibility.Visible;
        }

        public void HideBusyIndicator()
        {
            Overlay.Visibility = Visibility.Hidden;
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            Settings.Visibility = Visibility.Visible;
         
        }

        private void SaveSettingsButton_Click(object sender, RoutedEventArgs e)
        {
            Settings.Visibility = Visibility.Hidden;
            Properties.Settings.Default.CheckSvnModels = CheckSvnCheckbox.IsChecked ?? false;
            Properties.Settings.Default.AddNewModelToSvn = AutoAddToSvnCheckbox.IsChecked ?? false;
            Properties.Settings.Default.SelectedLook = ((ComboBoxItem)LookComboBox.SelectedItem).Content.ToString();
            Properties.Settings.Default.Save();
            _viewModel.SavedSettings();
        }
    }
}
