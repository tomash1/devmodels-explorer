﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevmodelsExplorer.ViewModel;

namespace DevmodelsExplorer.View
{
    /// <summary>
    /// Interaction logic for CreateModeLView.xaml
    /// </summary>
    public partial class CreateModeLView : Window
    {
        private readonly CreateModelViewModel _viewModel;

        public CreateModeLView(CreateModelViewModel viewModel)
        {
            InitializeComponent();
            viewModel.View = this;
            _viewModel = viewModel;
            DataContext = viewModel;
            _viewModel.FrontIconHeightTextBox = FrontIconHeightTextBox;
            _viewModel.BackIconHeightTextBox = BackIconHeightTextBox;
            _viewModel.FrontIconWidthTextBox = FrontIconWidthTextBox;
            _viewModel.BackIconWidthTextBox = BackIconWidthTextBox;
        }

        public void ShowSaveIndicator()
        {
            Overlay.Visibility = Visibility.Visible;
        }

        public void HideSaveIndicator()
        {
            Overlay.Visibility = Visibility.Hidden;
        }

        private static bool GetBoolValue(bool? nullableBool)
        {
            return nullableBool ?? false;
        }

        private Dictionary<string, object> CollectNewModelDataFromForm()
        {
            var manufacturer = _viewModel.GetManufacturerNameFromComboBox(ManufacturerComboBox);
            return new Dictionary<string, object>()
            {
                ["category"] = (string) CategoryComboBox.SelectedValue,
                ["depth"] = DepthTextBox.Text,
                ["height"] = HeightTextBox.Text,
                ["width"] = WidthTextBox.Text,
                ["heightInScrews"] = HeightInScrewsTextBox.Text,
                ["weight"] = WeightTextBox.Text.Replace(",", "."),
                ["model"] = ModelNameTextBox.Text,
                ["manufacturer"] = manufacturer,
                ["ratedCurrent"] = RatedCurrentTextBox.Text.Replace(",", "."),
                ["powerRated"] = PowerRatedTextBox.Text,
                ["heatRated"] = HeatRatedTextBox.Text,
                ["powerPortsTypeIN"] = (string) PowerPortsInComboBox.SelectedValue,
                ["powerPortsCountIN"] = PowerPortsInCountTextBox.Text,
                ["powerPortsTypeOUT"] = (string) PowerPortsOutComboBox.SelectedValue,
                ["powerPortsCountOUT"] = PowerPortsOutCountTextBox.Text,
                ["networkPortsCountCopper"] = CopperPortsCountTextBox.Text,
                ["networkPortsCountOptical"] = OpticalPortsCountTextBox.Text,
                ["imageFrontHeight"] = FrontIconHeightTextBox.Text,
                ["imageFrontWidth"] = FrontIconWidthTextBox.Text,
                ["imageBackHeight"] = BackIconHeightTextBox.Text,
                ["imageBackWidth"] = BackIconWidthTextBox.Text,
                ["rackMounted"] = GetBoolValue(RackMountedCategoryCheckBox.IsChecked),
                ["modularDevice"] = GetBoolValue(ModularDeviceCategoryCheckBox.IsChecked),
                ["modularFrontDevice"] = GetBoolValue(ModularDeviceFrontChechBox.IsChecked),
                ["modularBackDevice"] = GetBoolValue(ModularDeviceBackCheckBox.IsChecked),
                ["expansionModule"] = GetBoolValue(ExpansionModuleCategoryCheckBox.IsChecked),
                ["slotX"] = SlotXTextBox.Text,
                ["slotY"] = SlotYTextBox.Text
            };

        }

        private bool AreRequiredAttributesFilled(Dictionary<string, object> modelData)
        {
            var isManufacturerFilled = modelData["manufacturer"] != null &&
                                       ((string) modelData["manufacturer"]).Trim() != "";

            var isModelNameFilled = modelData["model"] != null &&
                                       ((string)modelData["model"]).Trim() != "";

            var isWidthFilled = modelData["width"] != null &&
                                       ((string)modelData["width"]).Trim() != "";

            var isHeightFilled = modelData["height"] != null &&
                                       ((string)modelData["height"]).Trim() != "";

            var isDepthFilled = modelData["depth"] != null &&
                                       ((string)modelData["depth"]).Trim() != "";

            var isWeightFilled = modelData["weight"] != null &&
                                       ((string)modelData["weight"]).Trim() != "";

            var isCategoryFilled = modelData["category"] != null &&
                                       ((string)modelData["category"]).Trim() != "";

            return isManufacturerFilled && isModelNameFilled && isWidthFilled && isHeightFilled && isDepthFilled && isWeightFilled && isCategoryFilled;
        }

        private void ButtonAddModel_OnClick(object sender, RoutedEventArgs e)
        {
            var modelData = CollectNewModelDataFromForm();
            if (!AreRequiredAttributesFilled(modelData))
                MessageBox.Show("Najpierw wypełnij wymagane pola...");
            else
            {

                _viewModel.ButtonAddModel_OnClick(modelData);
               
            }
        }

        private void ButtonFindFronIcon_OnClick(object sender, RoutedEventArgs e)
        {
            _viewModel.ButtonFindFronIcon_OnClick();
        }

        private void ButtonFindBackIcon_OnClick(object sender, RoutedEventArgs e)
        {
            _viewModel.ButtonFindBackIcon_OnClick();
        }

        private void OnlyDigit_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void ModularDeviceCategoryCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            ModularDeviceFrontChechBox.IsEnabled = true;
            ModularDeviceBackCheckBox.IsEnabled = true;
        }

        private void ModularDeviceCategoryCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            ModularDeviceFrontChechBox.IsEnabled = false;
            ModularDeviceBackCheckBox.IsEnabled = false;
            ModularDeviceFrontChechBox.IsChecked = false;
            ModularDeviceBackCheckBox.IsChecked = false;
        }

        private void ExpansionModuleCategoryCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            SlotXTextBox.IsEnabled = true;
            SlotYTextBox.IsEnabled = true;
        }

        private void ExpansionModuleCategoryCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            SlotXTextBox.IsEnabled = false;
            SlotYTextBox.IsEnabled = false;
            SlotXTextBox.Text = null;
            SlotYTextBox.Text = null;
        }

        private void ButtonCancelModel_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void HeightTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            var enteredHeight = HeightTextBox.Text;
            var parsedHeight = 0;
            if (!int.TryParse(enteredHeight, out parsedHeight)) return;
            const double r = 44.0 / 3.0;
            HeightInScrewsTextBox.Text = ((int)(parsedHeight / r)).ToString();
        }

        private void PowerRatedTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            HeatRatedTextBox.Text = PowerRatedTextBox.Text;
        }
    }
}
