﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace DevmodelsExplorer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;
        }

        private static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
            var e = (Exception) unhandledExceptionEventArgs.ExceptionObject;
            MessageBox.Show("Upsss... " + e.Message, "Coś poszło nie tak", MessageBoxButton.OK, MessageBoxImage.Error);
            
        }
    }
}
