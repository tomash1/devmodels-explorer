﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.VisualStyles;
using System.Windows.Threading;
using DevmodelsExplorer.Annotations;
using DevmodelsExplorer.Model;
using DevmodelsExplorer.Tools;
using DevmodelsExplorer.View;

using NewModelDataType = System.Collections.Generic.Dictionary<string, object>;

namespace DevmodelsExplorer.ViewModel
{
    public enum ModelCreationResult { Success, Fail, CannotCopyImages, CannotCopyModelFile, CannotAddModelToManufacturer }

    public class CreateModelViewModel : INotifyPropertyChanged
    {

        private static IEnumerable<string> GetCategories()
        {
            return ResourceManager.ReadFile("Assets\\Categories.txt");
        }

        private static IEnumerable<string> GetPowerOut()
        {
            return ResourceManager.ReadFile("Assets\\PowerPortsOut.txt");
        }

        private static IEnumerable<string> GetPowerIn()
        {
            return ResourceManager.ReadFile("Assets\\PowerPortsIn.txt");
        }

        #region Fields

        private readonly IEnumerable<string> _categories;
        private readonly IEnumerable<string> _powerOut;
        private readonly IEnumerable<string> _powerIn;
        private IEnumerable<string> _manufacturers;
        private CreateModeLView _view;

        private string _frontIconPath = "No file selected";
        private string _backIconPath = "No file selected";
        private TextBox _frontIconWidthTextBox;
        private TextBox _frontIconHeightTextBox;
        private TextBox _backIconWidthTextBox;
        private TextBox _backIconHeightTextBox;

        private readonly SvnManager _svnManager;
        private readonly CmdExecutor _cmdExecutor;

        #endregion

        #region Properties

        public TextBox FrontIconWidthTextBox {set { _frontIconWidthTextBox = value; } }
        public TextBox FrontIconHeightTextBox { set { _frontIconHeightTextBox = value; } }
        public TextBox BackIconWidthTextBox { set { _backIconWidthTextBox = value; } }
        public TextBox BackIconHeightTextBox { set { _backIconHeightTextBox = value; } }

        public IEnumerable<string> Categories => _categories;
        public IEnumerable<string> PowerOut => _powerOut;
        public IEnumerable<string> PowerIn => _powerIn;
        public IEnumerable<string> Manufacturers { get { return _manufacturers ?? new List<string>(); } set { _manufacturers = value; } }

        public string Depth { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string Width { get; set; }
        public string RatedCurrent { get; set; } = "0";
        public string HeatRated { get; set; } = "0";
        public string PowerPortsInCount { get; set; } = "0";
        public string PowerPortsOutCount { get; set; } = "0";
        public string CooperPortsCount { get; set; } = "0";
        public string OpticalPortsCount { get; set; } = "0";
        public string PowerRated { get; set; } = "0";
        public string SlotX { get; set; }
        public string SlotY { get; set; }
        public string Model { get; set; }
        public string Manufacturer { get; set; }
        public object Category { get; set; }
        public string FrontIconHeight { get; set; }
        public string FrontIconWidth { get; set; }
        public string BackIconHeight { get; set; }
        public string BackIconWidth { get; set; }

        public CreateModeLView View { set { _view = value; } }

        public DeviceManager DeviceManager { get; set; }
        public Action CreatedNewModelAction = null;

        public string FrontIconPath { get { return _frontIconPath; }
            set
            {
                _frontIconPath = value;
                OnPropertyChanged(nameof(FrontIconPath));
                var pxs = ImageManager.ResolveImageHeightAndWidthInPx(_frontIconPath);
                _frontIconHeightTextBox.Text = pxs["Height"].ToString();
                _frontIconWidthTextBox.Text = pxs["Width"].ToString();
            }
        }

        public string BackIconPath { get { return _backIconPath; }
            set
            {
                _backIconPath = value;
                OnPropertyChanged(nameof(BackIconPath));
                var pxs = ImageManager.ResolveImageHeightAndWidthInPx(_backIconPath);
                _backIconHeightTextBox.Text = pxs["Height"].ToString();
                _backIconWidthTextBox.Text = pxs["Width"].ToString();
                    
            }
        }

        #endregion

        #region Constructor

        public CreateModelViewModel(bool addNewModelToSvn)
        {
            _categories = GetCategories();
            _powerIn = GetPowerIn();
            _powerOut = GetPowerOut();

            _svnManager = new SvnManager(addToSvn: addNewModelToSvn);
            _cmdExecutor = new CmdExecutor(_svnManager);
        }

        #endregion

        #region Form Specific Data Getters

        public string GetManufacturerNameFromComboBox(AutoCompleteBox manufacturerComboBox)
        {
            var t = manufacturerComboBox.Text;
            return t.RemoveWhitespace().Equals("") ? null : t;
        }

        private string GetFrontIconFileName()
        {
            if (_frontIconPath == null) return null;
            return _frontIconPath.ToLower().StartsWith("no file") ? "" : Path.GetFileName(_frontIconPath);
        }

        private string GetBackIconFileName()
        {
            if (_backIconPath == null) return null;
            return _backIconPath.ToLower().StartsWith("no file") ? "" : Path.GetFileName(_backIconPath);
        }

        private string GetFrontIconFilePath()
        {
            if (_frontIconPath == null) return null;
            return _frontIconPath.ToLower().StartsWith("no file") ? "" : _frontIconPath;
        }

        private string GetBackIconFilePath()
        {
            if (_backIconPath == null) return null;
            return _backIconPath.ToLower().StartsWith("no file") ? "" : _backIconPath;
        }

        #endregion

        #region Add New Model Methods

        private void AddToModelDataAttributesStoredInViewModel(ref NewModelDataType modelData)
        {
            modelData["imageFrontFilePath"] = GetFrontIconFileName();
            modelData["imageBackFilePath"] = GetBackIconFileName();
        }

        private void AddToCreatedModelIconsData(ref CreatedModel createdModel)
        {
            createdModel.FrontIconPath = GetFrontIconFilePath();
            createdModel.FrontIconFileName = GetFrontIconFileName();
            createdModel.BackIconPath = GetBackIconFilePath();
            createdModel.BackIconFileName = GetBackIconFileName();
        }

        private static CreatedModel CreateModel(NewModelDataType modelData)
        {
            var builder = new DeviceIncBuilder(modelData);
            return builder.Build();
        }

        private string MoveModelFileToSvnPath(CreatedModel createdModel)
        {
            var toDir = DeviceManager.DmlPath + "\\" + SpecificNameManufacturer.GetSpecifigManufacturerNameLower(createdModel.Manufacturer);
            var movedToPath = _cmdExecutor.MoveFileToDirectory(createdModel.FileName, toDir);
            return movedToPath;
        }

        private void AddDeviceToExplorerView(string newDevicepath)
        {
            var createdDevice = DeviceManager.ParseDevice(newDevicepath);
            DeviceManager.AddDevice(createdDevice);
            CreatedNewModelAction?.Invoke();
        }

        private int CopyIconsToSvnFolder(CreatedModel createdModel)
        {
            if (!createdModel.isOneOfIconSet()) return 0;

            var toDirPath = ResourceManager.GetBlobPath() + "\\" + SpecificNameManufacturer.GetSpecificManufacturerNameForIcons(createdModel.Manufacturer);
            var imageCode = _cmdExecutor.CopyModelImagesToBlobDirectory(createdModel, toDirPath);
            if (imageCode != 0)
                return imageCode;

            if (createdModel.IsFrontIconSet())
                _svnManager.AddFileToSvn(createdModel.FrontIconPath);
            if (createdModel.IsBackIconSet())
                _svnManager.AddFileToSvn(createdModel.BackIconPath);
            return 0;
        }

        private void AddNewModel(NewModelDataType modelData)
        {
            var results = new List<ModelCreationResult>();

            AddToModelDataAttributesStoredInViewModel(ref modelData);
            var createdModel = CreateModel(modelData);
            if (createdModel == null)
            {
                results.Add(ModelCreationResult.Fail);
                _view.Dispatcher.Invoke(() => ShowSaveResults(results));
                return;
            }

            var movedToPath = MoveModelFileToSvnPath(createdModel);
            if (movedToPath == null)
                results.Add(ModelCreationResult.CannotCopyModelFile);

            createdModel.FilePath = movedToPath;
            _svnManager.AddFileToSvn(createdModel.FilePath);

            AddDeviceToExplorerView(createdModel.FilePath);
            if (!ManufacturerXmlManager.AddNewModel(createdModel, DeviceManager.DmlPath))
                results.Add(ModelCreationResult.CannotAddModelToManufacturer);

            AddToCreatedModelIconsData(ref createdModel);
            var iconCopyResult = CopyIconsToSvnFolder(createdModel);
            if (iconCopyResult != 0)
                results.Add(ModelCreationResult.CannotCopyImages);
            
            if (results.Count == 0)
                results.Add(ModelCreationResult.Success);

            _view.Dispatcher.Invoke(() => ShowSaveResults(results));
        }

        #endregion

        #region View Actions

        public void ButtonAddModel_OnClick(NewModelDataType modelData)
        {
            _view.ShowSaveIndicator();

            new Thread(
                () => AddNewModel(modelData)
                )
                .Start();
        }

        public void ButtonFindFronIcon_OnClick()
        {
            var path = ResourceManager.GetIconPath(FrontIconPath);
            if (path != null)
                FrontIconPath = path;
        }

        public void ButtonFindBackIcon_OnClick()
        {
            var path = ResourceManager.GetIconPath(BackIconPath);
            if (path != null)
                BackIconPath = path;
        }

        #endregion

        private void ShowSaveResults(IEnumerable<ModelCreationResult> results)
        {
            _view.HideSaveIndicator();
            foreach (var result in results)
            {
                switch (result)
                {
                    case ModelCreationResult.Success:
                        MessageBox.Show("Yupi ! Kolejny model gotowy :)");
                        break;
                    case ModelCreationResult.Fail:
                        MessageBox.Show("Ups.. Coś poszło nie tak. Więcej informacji w pliku error.txt");
                        break;
                    case ModelCreationResult.CannotCopyImages:
                        MessageBox.Show("Model dodany. Niestety nie udało się skopiować ikon...");
                        break;
                    case ModelCreationResult.CannotCopyModelFile:
                        MessageBox.Show(
                            "Model utworzony, ale nie udało się go przenieść do odpowiedniego katalogu. Musisz zrobić to ręcznie...");
                        break;
                    case ModelCreationResult.CannotAddModelToManufacturer:
                        MessageBox.Show(
                            "Model utworzony. Nie udało się dodać modelu do pliku zbiorczego producenta...");
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}


