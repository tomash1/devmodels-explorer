using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using DevmodelsExplorer.Annotations;
using DevmodelsExplorer.Model;
using DevmodelsExplorer.Tools;
using DevmodelsExplorer.View;
using Timer = System.Timers.Timer;

namespace DevmodelsExplorer.ViewModel
{
    internal class MainWindowViewModel : INotifyPropertyChanged
    {
        #region Fields

        private readonly MainWindow _view;
        private ParsingBusyIndicatorWindow _parsingBusyWindow;

        private readonly DeviceManager _deviceManager;
        private UserImage _frontIcon3D;
        private UserImage _backIcon3D;
        private UserImage _icon2D;
        private Uri _svnStatus;
        private Uri _settingsIconImage;
        private Uri _settingsIconImageOver;

        private ListBox _devicesListBox;
        private IEnumerable<Device> _filteredDevices = new List<Device>();

        private string _deviceModelName = "Wybierz model z listy";
        private string _manufacturerName = "";
        private string _implementsValues = "";
        private string _extendsValues = "";
        private string _selectedPath = "Wska� �cie�k� do folderu dml znajduj�cego sie w devmodels: devmodels/dml.";
        private string _svnToolTip = "";
        private string _busyIndicatorMessage = "";
        private string _icon3DFrontPath;
        private string _icon3DBackPath;
        private string _icon2DPath;
        private string _deviceFileName;

        private bool _is2DIconButtonEnabled = false;
        private bool _is3DFrontIconButtonEnabled = false;
        private bool _is3DBackIconButtonEnabled = false;

        public bool CheckSvnModels { get; set; }
        public bool AutoAddToSvn { get; set; }

        #endregion

        #region Properties
        public string SelectedPath { get { return _selectedPath; } set { _selectedPath = value; OnPropertyChanged(nameof(SelectedPath)); } }
        public ObservableCollection<Device> Devices => new ObservableCollection<Device>(_filteredDevices);
        public UserImage FrontIcon3D { get { return _frontIcon3D; } set { _frontIcon3D = value; OnPropertyChanged(nameof(FrontIcon3D)); } }
        public UserImage BackIcon3D { get { return _backIcon3D; } set { _backIcon3D = value; OnPropertyChanged(nameof(BackIcon3D)); } }
        public UserImage Icon2D { get { return _icon2D; } set { _icon2D = value; OnPropertyChanged(nameof(Icon2D)); } }

        public Uri SvnStatusIcon { get { return _svnStatus; } set { _svnStatus = value; OnPropertyChanged(nameof(SvnStatusIcon)); } }
        public Uri SettingsIconImage { get { return _settingsIconImage; } set { _settingsIconImage = value; OnPropertyChanged(nameof(SettingsIconImage)); } }
        public Uri SettingsIconImageOver { get { return _settingsIconImageOver; } set { _settingsIconImageOver = value; OnPropertyChanged(nameof(SettingsIconImageOver)); } }

        public string DeviceFileName { get { return _deviceFileName; } set { _deviceFileName = value; OnPropertyChanged(nameof(DeviceFileName)); } }
        public string DeviceModelName { get { return _deviceModelName; } set { _deviceModelName = value; OnPropertyChanged(nameof(DeviceModelName)); } }
        public string DeviceManufacturerName { get { return _manufacturerName; } set { _manufacturerName = value; OnPropertyChanged(nameof(DeviceManufacturerName)); } }
        public string DeviceImplements { get { return _implementsValues; } set { _implementsValues = value; OnPropertyChanged(nameof(DeviceImplements)); } }
        public string DeviceExtends { get { return _extendsValues; } set { _extendsValues = value; OnPropertyChanged(nameof(DeviceExtends)); } }
        public string SvnToolTip { get { return _svnToolTip; } set { _svnToolTip = value; OnPropertyChanged(nameof(SvnToolTip)); } }
        public string BusyIndicatorMessage { get {return _busyIndicatorMessage;} set { _busyIndicatorMessage = value; OnPropertyChanged(nameof(BusyIndicatorMessage)); } }

        public string FrontIcon3DPath { get { return _icon3DFrontPath ?? ""; }
            set
            {
                _icon3DFrontPath = value;
                OnPropertyChanged(nameof(FrontIcon3DPath));
                Icon3DFrontIsButtonEnabled = _icon3DFrontPath != "";
            } }
        public string BackIcon3DPath { get { return _icon3DBackPath ?? ""; }
            set
            {
                _icon3DBackPath = value;
                OnPropertyChanged(nameof(BackIcon3DPath));
                Icon3DBackIsButtonEnabled = _icon3DBackPath != "";
            } }
        public string Icon2DPath { get { return _icon2DPath ?? ""; }
            set
            {
                _icon2DPath = value;
                OnPropertyChanged(nameof(Icon2DPath));
                Icon2DIsButtonEnabled = _icon2DPath != "";
            } }

        public bool Icon2DIsButtonEnabled { get { return _is2DIconButtonEnabled; } set
        {
            _is2DIconButtonEnabled = value; OnPropertyChanged(nameof(Icon2DIsButtonEnabled));
        } }

        public bool Icon3DFrontIsButtonEnabled { get { return _is3DFrontIconButtonEnabled; } set
        {
            _is3DFrontIconButtonEnabled = value; OnPropertyChanged(nameof(Icon3DFrontIsButtonEnabled));
        } }

        public bool Icon3DBackIsButtonEnabled { get { return _is3DBackIconButtonEnabled; } set
        {
            _is3DBackIconButtonEnabled = value; OnPropertyChanged(nameof(Icon3DBackIsButtonEnabled));
        } }

        public ListBox DevicesListBox { set { _devicesListBox = value; } }

        public int SelectedLook { get; set; } = 0;

        private Thread _svnCheckThread = null;
        private Timer _svnCheckTimer = null;
        private SvnManager _svnManager;

        private bool _isTimerInitialized = false;

        private ParsingBusyIndicatorWindow ParsingBusyWindow
        {
            get
            {
                if (_parsingBusyWindow != null) return _parsingBusyWindow;
                _parsingBusyWindow = new ParsingBusyIndicatorWindow() {Owner = _view};
                return _parsingBusyWindow;
            }
        }

        #endregion

        #region Constructor

        public MainWindowViewModel(MainWindow view)
        {
            _deviceManager = new DeviceManager(view.Dispatcher);

            InitDefaultSettingsProperties();
            var isDarkSelected = SelectedLook == 0;

            SettingsIconImage = ResourceManager.GetImage(isDarkSelected ? "Settings-50.png" : "Settings Filled-50.png") ;
            SettingsIconImageOver = ResourceManager.GetImage(!isDarkSelected ? "Settings-50.png" : "Settings Filled-50.png");

            _view = view;
            _parsingBusyWindow = null;
        }

        private void InitDefaultSettingsProperties()
        {
            AutoAddToSvn = Properties.Settings.Default.AddNewModelToSvn;
            CheckSvnModels = Properties.Settings.Default.CheckSvnModels;
            SelectedLook = Properties.Settings.Default.SelectedLook == "Light" ? 1 : 0;
        }

        #endregion

        #region Svn Timer implementation

        private void ExecuteSvnCheck()
        {

            _svnManager.IsNewDataAvailable(result =>
            {
                switch (result)
                {
                    case SvnManager.SvnResult.Ok:
                        SvnStatusIcon = ResourceManager.GetImage("ThumbUp.png");
                        SvnToolTip = "Brak zmian na SVN";
                        break;
                    case SvnManager.SvnResult.Old:
                        SvnStatusIcon = ResourceManager.GetImage("New.png");
                        SvnToolTip = "Nowe modele na SVN";
                        break;
                    case SvnManager.SvnResult.CannotGetLocalRepo:
                    case SvnManager.SvnResult.SvnNotAvailable:
                        SvnStatusIcon = ResourceManager.GetImage("Sad.png");
                        SvnToolTip = "SVN nie jest aktualnie dost�pny";
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(result), result, null);
                }

            });

        }

        private void RunSvnCheckThread(object sender, ElapsedEventArgs args)
        {
            _svnCheckThread = new Thread(ExecuteSvnCheck);
            _svnCheckThread.Start();
        }

        private void InitSvnTimer()
        {
            if (_svnCheckTimer != null) return;

            _svnCheckTimer = new Timer(60000) { AutoReset = true };
            _svnCheckTimer.Elapsed += RunSvnCheckThread;
            _isTimerInitialized = true;
        }

        #endregion

        #region Devmodels Initialization

        private void NextDeviceIsParsedCallback(string deviceName)
        {
            if (deviceName == null)
                AllDevicesAreParsed();

            ParsingBusyWindow.UpdadeProgressBar(deviceName);
        }

        private void AllDevicesAreParsed()
        {
            ParsingBusyWindow.Hide();
            _parsingBusyWindow = null;

            if (_deviceManager.AreDevicesFound())
            {
                _filteredDevices = _deviceManager.Devices;
                OnPropertyChanged(nameof(Devices));
            }
            else
                MessageBox.Show("Nie znaleziono urz�dze�");
        }

        private static bool IsDmlPathCorrect(string dmlPath)
        {
            return dmlPath.TrimEnd().EndsWith("dml");
        }

        private void ReadDevicesInWorkingCopy(string path)
        {
            _deviceManager.FindIncsInPath(path);
            _deviceManager.ReadDevicesFromFoundIncs(NextDeviceIsParsedCallback);

            var incsCount = _deviceManager.IncsCount;
            ParsingBusyWindow.Max = (incsCount - 200) / DeviceManager.CallbackCallFrequency; // all incs - manufacturer xslt files
            ParsingBusyWindow.ShowDialog();
        }

        #endregion

        #region Svn Integration

        private void DoSvnCheck()
        {
            if (_svnManager == null)
                _svnManager = new SvnManager(_view.Dispatcher, SelectedPath);

            BusyIndicatorMessage = "Sprawdzam repozytorium SVN...";
            _view.ShowBusyIndicator();

            InitSvnTimer();
            _svnCheckTimer.Enabled = true;

            _svnManager.IsNewDataAvailable(CheckSvnResult);
        }

        private void CheckSvnResult(SvnManager.SvnResult result)
        {
            switch (result)
            {
                case SvnManager.SvnResult.Ok:
                    ReadDevicesInWorkingCopy(SelectedPath);
                    _view.HideBusyIndicator();
                    break;
                case SvnManager.SvnResult.Old:
                    SvnIsNotUpdatedReaction();
                    break;
                case SvnManager.SvnResult.CannotGetLocalRepo:
                    EnteredNotSvnPath(SelectedPath);
                    _view.HideBusyIndicator();
                    break;
                case SvnManager.SvnResult.SvnNotAvailable:
                    SvnIsNotAvailable(SelectedPath);
                    _view.HideBusyIndicator();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SvnIsNotAvailable(string path)
        {
            if (MessageBox.Show("Nie mo�na po��czy� si� z repozytorium SVN. Kontynuowa� mimo to?", "Przemy�l..", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                ReadDevicesInWorkingCopy(path);
            }
        }

        private void EnteredNotSvnPath(string path)
        {
            if (MessageBox.Show("Wskaza�e� �ci�k�, kt�ra nie ma �r�de� z svn. Jeste� pewien, �e chcesz jej u�y�?", "Przemy�l..", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                ReadDevicesInWorkingCopy(path);
            }
        }

        private void CheckSvnUpdateResultAndReact(bool isUpdatedWithSuccess)
        {
            if (isUpdatedWithSuccess)
                ReadDevicesInWorkingCopy(SelectedPath);
            else
                MessageBox.Show("Nie uda�o si� uaktualni� SVN. Musisz zrobi� to r�cznie");

            _view.HideBusyIndicator();
        }

        private void SvnIsNotUpdatedReaction()
        {
            if (
                MessageBox.Show("S� dost�pne nowe model. Czy wykon�� update ?", "Przemy�l...", MessageBoxButton.YesNo,
                    MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                BusyIndicatorMessage = "Uaktualniam kopi� robocz� SVN...";
                _svnManager.UpdateSvnRoot(CheckSvnUpdateResultAndReact);
            }
            else
            {
                _view.HideBusyIndicator();
                MessageBox.Show("Wykonaj update r�cznie przed dodaniem modeli...");
            }
        }

        #endregion

        #region Filters

        public void FilterDevicesByModelName(string filterQuery)
        {
            _filteredDevices = _deviceManager.FilterDevicesByModel(filterQuery);
            OnPropertyChanged(nameof(Devices));
        }

        public void FilterDevicesByManufacturerName(string filterQuery)
        {
            _filteredDevices = _deviceManager.FilterDeviceByManufacturer(filterQuery);
            OnPropertyChanged(nameof(Devices));
        }

        #endregion

        #region View Actions

        public void ListBoxSelectionChanged()
        {
            var d = _devicesListBox.SelectedItem as Device;
            if (d == null) return;

            DeviceModelName = d.Name;
            DeviceManufacturerName = d.Manufacturer;
            DeviceImplements = d.Implements;
            DeviceExtends = d.Extends;
            DeviceFileName = d.FilePath;
            FrontIcon3D = d.FrontIcon3D;
            BackIcon3D = d.BackIcon3D;
            Icon2D = d.Icon2D;
            FrontIcon3DPath = d.FrontIcon3D != null ? d.FrontIcon3D.FileName : "";
            BackIcon3DPath = d.BackIcon3D != null ? d.BackIcon3D.FileName : "";
            Icon2DPath = d.Icon2D != null ? d.Icon2D.FileName : "";
        }

        public void ListBoxDoubleClicked()
        {
            var d = _devicesListBox.SelectedItem as Device;
            if (d != null)
                CmdExecutor.SelectPathInWindowsExplorer(d.FilePath);
        }

        public void OpenDevmodelsButton_Click(bool isSvnCheckEnabled)
        {
            var workingDirectoryDmlPath = ResourceManager.GetDirectoryPathFromOpenDialog();
            if (workingDirectoryDmlPath == null) return;

            if (!IsDmlPathCorrect(workingDirectoryDmlPath))
            {
                MessageBox.Show("Podana �cie�ka nie jest poprawna...");
                return;
            }

            Properties.Settings.Default.SelectedPath = SelectedPath = workingDirectoryDmlPath;
            if (isSvnCheckEnabled)
                DoSvnCheck();
            else
                ReadDevicesInWorkingCopy(workingDirectoryDmlPath);
        }

        public void Icon2dPathOpen_Click()
        {
            var d = _devicesListBox.SelectedItem as Device;
            if (d?.Icon2D != null)
            {
                CmdExecutor.SelectPathInWindowsExplorer(d.Icon2D.Path);
            }
        }

        public void Icon3dFrontPathOpen_Click()
        {
            var d = _devicesListBox.SelectedItem as Device;
            if (d?.FrontIcon3D != null)
            {
                CmdExecutor.SelectPathInWindowsExplorer(d.FrontIcon3D.Path);
            }
        }

        public void Icon3dBackPathOpen_Click()
        {
            var d = _devicesListBox.SelectedItem as Device;
            if (d?.BackIcon3D != null)
            {
                CmdExecutor.SelectPathInWindowsExplorer(d.BackIcon3D.Path);
            }
        }

        private void CreatedNewModel()
        {
            OnPropertyChanged(nameof(Devices));
        }

        public void AddNewModel_Click()
        {
            var manufacturers = _deviceManager.Manufacturers;
            if (!manufacturers.Any())
            {
                MessageBox.Show("Najpierw za�aduj istniej�ce modele");
                return;
            }

            var isAddToSvnEnabled = Properties.Settings.Default.AddNewModelToSvn;
            var viewModel = new CreateModelViewModel (isAddToSvnEnabled)
            {
                Manufacturers = manufacturers,
                DeviceManager = _deviceManager,
                CreatedNewModelAction = CreatedNewModel
            };

            var view = new CreateModeLView(viewModel);
            view.Show();
        }

        #endregion

        #region Application Look

        private void ChangeApplicationLook()
        {
            var toRemove = Application.Current.Resources.MergedDictionaries
                .Where(mergedDict => mergedDict.Source.ToString().Contains("Metro"))
                .ToList();

            foreach (var resourceDictionary in toRemove)
                Application.Current.Resources.MergedDictionaries.Remove(resourceDictionary);
            
            InitAppLook();
        }

        public void InitAppLook()
        {
            var look = Properties.Settings.Default.SelectedLook;
            var dict1 = new ResourceDictionary();
            var dict2 = new ResourceDictionary();
            if (look == "Dark")
            {
                dict1.Source = new Uri("Themes/MetroDark/MetroDark.MSControls.Core.Implicit.xaml", UriKind.Relative);
                dict2.Source = new Uri("Themes/MetroDark/MetroDark.MSControls.Toolkit.Implicit.xaml", UriKind.Relative);
            }
            else
            {
                dict1.Source = new Uri("Themes/Metro/Metro.MSControls.Core.Implicit.xaml", UriKind.Relative);
                dict2.Source = new Uri("Themes/Metro/Metro.MSControls.Toolkit.Implicit.xaml", UriKind.Relative);
            }
            Application.Current.Resources.MergedDictionaries.Add(dict1);
            Application.Current.Resources.MergedDictionaries.Add(dict2);
        }
        #endregion

        #region User settings update

        private void StopCheckingSvn()
        {
            _svnCheckThread?.Abort();
            SvnToolTip = "";
            SvnStatusIcon = null;
            _svnCheckTimer.Enabled = false;
        }

        private void AdjustCheckSvnModelsSelection()
        {
            if (!CheckSvnModels)
                StopCheckingSvn();
            else if (_isTimerInitialized)
            {
                _svnCheckTimer.Enabled = true;
                RunSvnCheckThread(this, null);
            }
        }

        public void SavedSettings()
        {
            AutoAddToSvn = Properties.Settings.Default.AddNewModelToSvn;
            CheckSvnModels = Properties.Settings.Default.CheckSvnModels;
            SelectedLook = Properties.Settings.Default.SelectedLook == "Light" ? 1 : 0;

            ChangeApplicationLook();
            AdjustCheckSvnModelsSelection();
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

